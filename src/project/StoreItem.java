// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 25, 2022; 	
// name of project: store project






package project;

public class StoreItem {
	
	private String name;
	private double price;
	
	
	public StoreItem() {
		name = "item";
		price = 0.0;

	}
	
	
	public StoreItem(String name, double price) {
		setName(name);
		setPrice(price);

	}
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	

	
	public void setName(String name) {
		if (validName(name)) {
			this.name = name;
		}
	}
	

	
	
	public void setPrice(double price) {
		if (validPrice(price)) {
			this.price = price;
		}
	}
	
	public String toString() {
		return  name + "," + price;
	}
	
	public boolean validName(String name) {
		if (name.length() < 30) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validPrice(double price) {
		if (price >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof StoreItem)) {
			return false;
		}
		
		StoreItem s = (StoreItem)obj;
		
		if (this.name != s.getName() || this.price != s.getPrice() ) {
			return false;
		}
		
		return true;
	}
}
