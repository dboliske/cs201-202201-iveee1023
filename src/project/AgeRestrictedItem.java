// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 25, 2022; 	
// name of project: store project



package project;

public class AgeRestrictedItem extends StoreItem {

	private int requiredAge;
	
	public AgeRestrictedItem() {
		super();
		this.requiredAge = 18;
	}
	
	public AgeRestrictedItem(String name, double price, int requiredage) {
		super(name, price);
		setRequiredage(requiredage);
	}
	
	public void setRequiredage(int age) {
		if (validRequiredage(age)) {
			this.requiredAge = age;
		}
	}
	
	public boolean validRequiredage(int age) {
		if (age > 0 && age < 130) {
			return true;
		}
		return false;
	}
	
	public int getRequiredage() {
		return this.requiredAge;
	}
	
	public String toString() {
		return  super.toString() + "," + requiredAge;
	}
	
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem a = (AgeRestrictedItem)obj;
		if (this.requiredAge != a.getRequiredage()) {
			return false;
		}
		
		return true;
	}
}
