// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 25, 2022; 	
// name of project: store project





package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class StoreApp {

	//main function
	public static void main(String[] args) {

		//calling readFile helper function to get the data prepared.
		StoreItem[] stocks = readFile("./src/project/stock.csv");
		sort(stocks);
		
		try {
			menu(stocks);
		} catch (Exception e) {
			System.out.println("Loading Error");
		}
		
	}
	
	
	
	// menu
	public static void menu(StoreItem[] stocks) {
		
		
		
		ArrayList<StoreItem> cart = new ArrayList<>();


		
		Scanner scanner = new Scanner(System.in);
		boolean run = true;

		
		do {
			System.out.println("----------- HERE IS THE MAIN MENUE ----------- ");
			System.out.println("1. Display all store items");
			System.out.println("2. Shopping --- Add to Cart (Warning: add to cart does not change stocks");
			System.out.println("3. Update Stocks -- Add Item or Remove Item");
			System.out.println("4. Search Product by Name");
			System.out.println("5. Display cart and Checkout");
			System.out.println("6. Save and Exit");
			String choice = scanner.nextLine();
			switch (choice) {
				case "1":
					displayStoreItems(stocks); 
					break;
				case "2":
					cart = addToCart(stocks, scanner);
					break;
				case "3":
					stocks = updateStocks(stocks, scanner);
					break;
				case "4":
					searchItem(stocks, scanner);
					break;
				case "5":
					stocks = displayCart(stocks, cart, scanner);
					break;
				case "6":
					saveFile(stocks, scanner);
					run = false;
					break;
				
				default:
					System.out.println("Invalid");	
			}
		} while (run);
		
		System.out.println("See you next time!");		
		scanner.close();
		
	}
		
		
	
	
	
	public static void displayStoreItems(StoreItem[] stocks) {
		int count = 0;
		
		//Hashmap is for showing same products together
		HashMap<String, Integer> map = new HashMap<>();
		
		for (int i = 0; i < stocks.length; i++) {
			String name = stocks[i].getName();
			if (map.containsKey(name)) {
				map.put(name, map.get(name) + 1);
			} else {
				if (!map.containsKey(name)) {
					map.put(name, 1);
				}
			}
		}
		
		try {
			for (Map.Entry<String, Integer> set :map.entrySet()) {
				System.out.println(count+ 1 + " Name: " + set.getKey() + "/ Stock: " + set.getValue());
				for (Object obj : stocks) {
					if (obj instanceof AgeRestrictedItem && ((AgeRestrictedItem) obj).getName() == set.getKey()) {
						AgeRestrictedItem a = (AgeRestrictedItem)obj;
						System.out.println("Price:  $" + a.getPrice());
						System.out.println("Required Age:  " + a.getRequiredage());
					} else if (obj instanceof ProduceItem && ((ProduceItem) obj).getName() == set.getKey()) {
						ProduceItem p = (ProduceItem)obj;
						System.out.println("Price:  $" + p.getPrice());
						System.out.println("Expire Date: " + p.getExpiredate());
					} else if (obj instanceof StoreItem && ((StoreItem) obj).getName() == set.getKey()) {
						StoreItem s = (StoreItem)obj;
						System.out.println("Price:  $" + s.getPrice());
					}
				}
				System.out.println(" ");
				count++;
			}
			
			 
		} catch (Exception e) {
			System.out.println("Cant read");
		}
		
		System.out.println("Total " + count + " choices");
		//reminder for produce product
		System.out.println("Reminder: Today is " + LocalDate.now());
		
		
		
	}
	
	
	// add desire item to the cart
	public static ArrayList<StoreItem> addToCart(
			StoreItem[] stocks, 
			Scanner scanner
			) {
		
		ArrayList<StoreItem> cart = new ArrayList<>();
		
		sort(stocks);
		displayStoreItems(stocks);
		boolean run = true;
		
		do {
			System.out.println("Which one you wish to add to the cart? or type quit to back to the main menu");
			String choice = scanner.nextLine();
			
			switch (choice) {
				case "quit":
					run = false; 
					break;
				default:
					int index = search(stocks, choice);
					
					if (index == -1) {
						System.out.println(choice + " not found. Or had been sold out.");
					} else {
						
							System.out.println(choice + " added.");
							cart.add(stocks[index]);
							stocks = removeStocks(stocks, choice);
							displayStoreItems(stocks);
			

					}

			}
		} while (run);
		
		
		
		return cart;
	}
	
	
	
	
	
	
	
	
	
	// Shopping functions
	
	
	//Display cart item.
	public static StoreItem[] displayCart(StoreItem[] stocks, ArrayList<StoreItem> cart, Scanner scanner) {	
		if (cart.size() > 0) {
			HashMap<String, Integer> map = new HashMap<>();
			
			for (int i = 0; i < cart.size(); i++) {
				String name = cart.get(i).getName();
				if (map.containsKey(name)) {
					map.put(name, map.get(name) + 1);
				} else {
					if (!map.containsKey(name)) {
						map.put(name, 1);
					}
				}
			}
			
			for (Map.Entry<String, Integer> set :map.entrySet()) {
				System.out.println("Name: " + set.getKey() + " ------ " + set.getValue());
			} 
			
		}
		System.out.println("There is total " + cart.size() + " items in cart.");
		
		double amount = 0;
		
		for (StoreItem s : cart) {
			amount += s.getPrice();
		}
		
		System.out.println("Your total is $" + amount);
		
		
		stocks = checkoutCart(stocks, cart, scanner);

		return stocks;
		
	}
	
	
	// Checkout process
	public static StoreItem[] checkoutCart(StoreItem[] stocks, ArrayList<StoreItem> cart, Scanner scanner) {
		int min_age = Integer.MIN_VALUE;
		boolean ageItem = false;
	
		for (StoreItem obj : cart) {
			if (obj instanceof AgeRestrictedItem ) {
				AgeRestrictedItem a = (AgeRestrictedItem)obj;
				min_age = Math.max(min_age, a.getRequiredage());
				ageItem = true;
			}
			
		}
		
		boolean run = true;
		
		do {
			System.out.println("Sure to checkout? y/n");
			String choice = scanner.nextLine();
			
			switch (choice) {
			case "y":
				if (ageItem == true) {
					System.out.println("What is your age?");
					try {
						int age = Integer.parseInt(scanner.nextLine());
						if (age >= min_age) {
							for (StoreItem s : cart) {
								stocks = removeStocks(stocks, s.getName());
							}
							System.out.println("Thanks for shopping");
							return stocks;
						} else if (age < min_age ){
							System.out.println("Sorry, you are underaged. The miminal age to purchase is " + min_age);
						} 
					} catch(Exception e) {
						System.out.println("Invalid Input");
					}
					
				} else {
					for (StoreItem s : cart) {
						stocks = removeStocks(stocks, s.getName());
					}
					System.out.println("Thanks for shopping");
					return stocks;
				}
				
				
			case "n":
				System.out.println("Thanks for your time");
				run = false;
				break;
			default:
				System.out.println("Invalid Input");
			
			} 
		} while (run);

		return stocks;
	}
	
	
	
	
	
	//Search item function, calling binary search function
	
	public static void searchItem(StoreItem[]stocks, Scanner scanner) {
		
		sort(stocks);
		
		System.out.println("search what?");
		String value = scanner.nextLine();
		int index = search(stocks, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at stock " + index + ".");
		}

	}
	

	
	
	// remove stocks
	public static StoreItem[] removeStocks(StoreItem[] stocks, String name) {
		int index = search(stocks, name);
		
		
		StoreItem[] copy = new StoreItem[stocks.length - 1];

		for (int i = 0, j = 0; i < stocks.length; i++) {
		    if (i != index) {
		        copy[j++] = stocks[i];
		    }
		}
		
		return copy;
		
	}
	
	
	
	
	
	
	// update Stocks sub menu
	
	public static StoreItem[] updateStocks(StoreItem[] stocks, Scanner scanner) {
		
		boolean run = true;
		
		do {
			System.out.println("Here is the menu: ");
			System.out.println("1. add shelf item");
			System.out.println("2. add age restricted item");
			System.out.println("3. add produce item");
			System.out.println("4. Remove Items");
			System.out.println("5. update items");
			System.out.println("6. quit");

			String choice = scanner.nextLine();
			switch (choice) {
				case "1":
					stocks = addShelfItem(stocks, scanner); 
					break;
				case "2":
					stocks = addAgeRestrictedItem(stocks, scanner);
					break;
				case "3":
					
					stocks = addProduceItem(stocks, scanner);
					break;
				case "4":
					System.out.println("name?");
					try {
						String name = scanner.nextLine();
						stocks = removeStocks(stocks, name);
						System.out.println("1 " + name + " has been removed");
					} catch (Exception e) {
						System.out.println("Not Found");
					}
				case "5":
					System.out.println("name?");
					try {
						String name = scanner.nextLine();
						stocks = updateItem(stocks, scanner, name);
					} catch (Exception e) {
						System.out.println("Not Found");
					}
				case "6":
					run = false;
					break;
				default:
					System.out.println("Invalid");	
			}
		} while (run);
		
		
		return stocks;
	};
	
	
	
	// update specific item
	
	public static StoreItem[] updateItem(StoreItem[] stocks, Scanner scanner, String name) {
		int i = search(stocks, name);
		System.out.println("Here is the item " + stocks[i]);
		StoreItem obj = stocks[i];
	
		
		System.out.println("Which attribute you wish to change?");
		String choice = scanner.nextLine();
		
		switch (choice) {
			case "name":
				System.out.println("desired new name?");
				String new_name = scanner.nextLine();
				for (int k = 0; k < stocks.length; k++) {
					if (stocks[k].getName().equalsIgnoreCase(name)) {
						stocks[k].setName(new_name);
					}	
				}
				System.out.println(name + " has been updated");
				break;
			case "price":
				System.out.println("desired new price?");
				try {
					double price = Double.parseDouble(scanner.nextLine());
					for (int k = 0; k < stocks.length; k++) {
						if (stocks[k].getName().equalsIgnoreCase(name)) {
							stocks[k].setPrice(price);
						}	
					}
					System.out.println(name + " has been updated");
				} catch (Exception e) {
					System.out.println("invalid input");
				}
				break;
			case "expire date":
				LocalDate date = null;
				if (obj instanceof ProduceItem) {
					System.out.println("desired new date? in yyyy-mm-dd formate");
					try {
						date = LocalDate.parse(scanner.nextLine());
					} catch (Exception e) {
						System.out.println("invalid input");
					}
					
					for (int k = 0; k < stocks.length; k++) {
						if (stocks[k].getName().equalsIgnoreCase(name)) {
							ProduceItem p = (ProduceItem)stocks[k];
							p.setExpireday(date);
						}	
					}
					System.out.println(name + " has been updated");
				} else {
					System.out.println("Do not have this attribute");
				}
				break;
			case "required age":
				int age = 0;
				if (obj instanceof AgeRestrictedItem) {
					System.out.println("desired new age?");
					try {
						age = Integer.parseInt(scanner.nextLine());
					} catch (Exception e) {
						System.out.println("invalid input");
					}
					
					for (int k = 0; k < stocks.length; k++) {
						if (stocks[k].getName().equalsIgnoreCase(name)) {
							AgeRestrictedItem a = (AgeRestrictedItem)stocks[k];
							a.setRequiredage(age);
						}	
					}
					System.out.println(name + " has been updated");
				} else {
					System.out.println("Do not have this attribute");
				}

				break;
			default:
				System.out.println("invalid input");
		}

		
		return stocks;
		
	}
	
	
	
	
	
	
	// add new shelf items
	public static StoreItem[] addShelfItem(StoreItem[] stocks, Scanner scanner) {
		
		
		try {
		System.out.println("name?");
		String name = scanner.nextLine();
		System.out.println("price?");
		
		
		double price = Double.parseDouble(scanner.nextLine());
		int count = stocks.length;
			
		StoreItem s = new StoreItem (name, price);
			  
		stocks = resize(stocks, count * 2); 
		stocks[count] = s;
		stocks = resize(stocks, count+1); 
			
		System.out.println("Added!");
			
			
		} catch (Exception e) {
			System.out.println("Invalid Input");
		}
		
		return stocks;
	}
	
	// add new age restricted item
	public static StoreItem[] addAgeRestrictedItem(StoreItem[] stocks, Scanner scanner) {
		
		
		try {
		System.out.println("name?");
		String name = scanner.nextLine();
		
		
		System.out.println("price?");
		Double price = Double.parseDouble(scanner.nextLine());
		System.out.println("Restricted age?");
		int age = Integer.parseInt(scanner.nextLine());
		
		
		int count = stocks.length;
		
		AgeRestrictedItem a = new AgeRestrictedItem (name, price, age);
		  
		stocks = resize(stocks, count * 2); 
		stocks[count] = a;
		stocks = resize(stocks, count + 1); 
		
		System.out.println("Added!");
		
		} catch (Exception e) {
			
			System.out.println("Invalid Input");
		}

		
		return stocks;
	}
	
	
	
	// add new produce item
	public static StoreItem[] addProduceItem(StoreItem[] stocks, Scanner scanner) {
		
		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		
		
		try {
		
		System.out.println("name?");
		String name = scanner.nextLine();
		
		System.out.println("price?");
		Double price = Double.parseDouble(scanner.nextLine());
		
		System.out.println("Expire Date? in mm/dd/yyyy format please");
		LocalDate date = LocalDate.parse(scanner.nextLine(), dateTimeFormatter);
				
				
		
		
		int count = stocks.length;
		
		ProduceItem p = new ProduceItem (name, price, date);
		  
		stocks = resize(stocks, count * 2); 
		stocks[count] = p;
		stocks = resize(stocks, count+1); 
		
		System.out.println("Added!");
		 
		} catch (Exception e) {
			System.out.println("Invalid Input");
		}
		
		return stocks;
	}
	
	
	

	
	
	// saveFile when quit
	
	public static void saveFile(StoreItem[] stocks, Scanner scanner) {

		
	    // save it to the file
	    try {
	    	//open a new file, or write into a existing file
	    	FileWriter f = new FileWriter("./src/project/stock.csv");
	    	
	    	// write into the file, line by line.
	    	for (int i = 0; i < stocks.length; i++) {
	    		f.write(stocks[i] + "\n");
	    	}
	    	
	    	// save and close.
	    	f.flush();
	    	f.close();
	    	
	    } catch (IOException e) {
	    	// catch error
	    	System.out.println(e.getMessage());
	    }
	    
	    // notice the user that their file has been saved.
	    System.out.println("Finished writing file, please check the file");
	}
	
	

	
	// helper functions
	
	
	
	// ReadFile function, help to prepare the data
	public static StoreItem[] readFile(String filename) {
		StoreItem[] stocks = new StoreItem[5];
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		int count = 0;
		
		
		try {
			File f = new File(filename);
			Scanner file = new Scanner(f);
			//parsing data line by line
			while (file.hasNextLine()) {
				String line = file.nextLine();
				String[] values = line.split(",");			
				
				if (values.length == 2) {
					StoreItem s = new StoreItem (
							values[0],
							Double.parseDouble(values[1])
							);
					
					if (count == stocks.length) {  
						stocks = resize(stocks, count * 2); 
					}
					
					stocks[count] = s;
					count++;
					
				} else if (values.length == 3 && values[2].length() <= 2) {
					AgeRestrictedItem a = new AgeRestrictedItem (
							values[0],
							Double.parseDouble(values[1]),
							Integer.parseInt(values[2])
							);
					
					if (count == stocks.length) {  
						stocks = resize(stocks, count * 2); 
					} 
					
					stocks[count] = a;
					count++;
					
					
				} else if (LocalDate.parse(values[2], dateTimeFormatter ) != null) {
					ProduceItem p = new ProduceItem(
							values[0],
							Double.parseDouble(values[1]),
							LocalDate.parse(values[2], dateTimeFormatter)
							);
					
					if (count == stocks.length) {  
						stocks = resize(stocks, count * 2); 
					}
					
					stocks[count] = p;
					count++;
					
				
				}
			}
			
			file.close();
			stocks = resize(stocks, count);
		
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (Exception e) {
			System.out.println("cant read");
		}
		
		return stocks;
	}
	
	
	// resize function, help to read and write data.
	public static StoreItem[] resize(StoreItem[] data, int size)
	{
		StoreItem[] temp = new StoreItem[size];  // create a temp array for future copy use
		int limit = data.length > size ? size : data.length; 
		for(int i=0; i<limit; i++) {
			temp[i] = data[i]; // copy the temp array
		}

		return temp;
	}
	
	
	// binary search 
	public static int search(StoreItem[] array, String value) {
		
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end) / 2;
			if (array[middle].getName().equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].getName().compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return pos;
		
		
	}
	
	
	// bubble sort
	public static StoreItem[] sort(StoreItem[] array) {
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<array.length - 1; i++) {
				if (array[i+1].getName().compareTo(array[i].getName()) < 0) {
					StoreItem temp = array[i+1];
					array[i+1] = array[i];
					array[i] = temp;
					done = false;
				}
			}
		} while (!done);
		
		return array;
	}
	

}
