     

Describe the user interface. What are the menu options and how will the user use the application?

	1.  Create item and add new items to the store stock.
		1.   If it is a new product then create a new product and add it	
		2.   If it is an exist product, just add the amount of the product.
    2.  Chosee items and add it to the cart
    3.  Choose item and the amount needed
    4.  Checkout function, calculate total, and then remove the items.
    5.  Search for items by names
    6.  Updating store stocks
    7.   Exit

Describe the programmers' tasks:

	1. Describe how you will read the input file.
			-  I will use file scanner to read through an excel list, each line represent a object
	2. Describe how you will process the data from the input file.
		   - read throught the input file and save it to an ArrayList
	3.  Describe how you will store the data (what objects will you store?)
		   - store the data to ArrayList before the user decided to exit the program.
	4. How will you add/delete/modify data?
			- use add/delete/modify functions to manipulate the arraylist.
	5. How will you search the data?
			-   I will use binary search to find exact item needed.
	6. List the classes you will need to implement your application.
			-  StoreItem
			-  AgeRestrictedGoods
			-  ProduceGoods
			-  StoreApp


Draw a UML diagram that shows all classes in the program and their relationships. This can be done however you want. If you want a specific application, [StarUML](http://staruml.io/download) and [Draw.io](https://draw.io/) are both good. But you are welcome to use any graphics program or just draw them by hand and scan them.  
 - please refer to design.png in this folder
 
 
Think how you will determine if your code functions are expected. Develop a test plan based on the above description; how will you test that the expected outputs have been achieved for every menu option? Be sure this test plan is complete. Your test plan should minimally test each option in the menu-driven user interface for expected behavior as well as error-handling. Your test plan should be in a spreadsheet format (preferably Excel, CSV, or TSV).
- Please refer to the testplan.csv file in this folder
 
