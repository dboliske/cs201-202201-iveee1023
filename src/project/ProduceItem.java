// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 25, 2022; 	
// name of project: store project




package project;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ProduceItem extends StoreItem {
	
	private LocalDate expireDate;
	
	public ProduceItem() {
		super();
		LocalDate current = LocalDate.now();
		this.expireDate = current.plusDays(3);
	}
	
	public ProduceItem(String name, double price, LocalDate expiredate) {
		super(name, price);
		setExpireday(expiredate);
	}
	
	public void setExpireday(LocalDate expiredate) {
		if (validExpiredate(expiredate)) {
			this.expireDate = expiredate;
		} 
	}
	
	public LocalDate getExpiredate() {
		return this.expireDate;
	}
	
	public boolean validExpiredate(LocalDate expiredate) {
		if (expiredate.compareTo(LocalDate.now()) >= 0) {
			return true;
		}
		return false;
	}
	
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
        String localDate = formatter.format(expireDate);
		return super.toString() + "," + localDate;
	}
	
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ProduceItem)) {
			return false;
		}
		
		ProduceItem p = (ProduceItem)obj;
		if (this.expireDate != p.getExpiredate()) {
			return false;
		}
		
		return true;
	}
	
	
	
	
}
