package exams.first;

public class Pet {
	
	
	//private variable name and age
	private String name;
	private int age;
	
	
	// Constructor method - default
	public Pet() {
		//default variable
		name = "pet";
		age = 1;
	}
	
	
	// constructor method which takes arguments
	public Pet(String name, int age) {
		// assign the given arguments to variable name
		this.name = name;
		// check if the age input is positive, otherwise, we will assign a default positive variable
		if (age > 0 ) {
			this.age = age;
		} else {
			this.age = 1;
		}
	}
	
	// mutators to set name
	public void setName(String name) {
		// assign the given arguments to variable name
		this.name = name;
	}
	
	// mutators to set age
	public void setAge(int age) {
		// check if the age input is positive, if it is not, ignore it.
		if (age > 0 ) {
			this.age = age;
		} 
	}
	
	// access to the name variable
	public String getName() {
		return name;
	}
	
	// access to the age variable
	public int getAge() {
		return age;
	}
	
	// check if the two instances are equal, return boolean.
	public boolean equals(Pet p) {
		if(this.name == p.name && this.age == p.age) {
			return true;
		}
		
		return false;
	}
	
	
	// print out all variables.
	public String toString() {
		return "( " + name + " ," + age + " )";
	}
}
