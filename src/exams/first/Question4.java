package exams.first;

import java.util.Scanner;

public class Question4 {

	public static void main(String[] args) {
		// create a string array with 5 slots for user's input 
		String[] words = new String[5];
		// create a string array for duplicate word, the maximum possible repetition is 2.
		String[] duplicate = new String[2];
		
		
		// Create a scanner for user's input
		Scanner scanner = new Scanner(System.in);
		
		// loop for 5 times to ask user's input
		for (int a=0; a < words.length; a++) {
			// ask user for his/her input
			System.out.println("Give me a word");
			// save user's input into the array
			words[a] = scanner.nextLine();

		}
		
		
		// loop to compare, the outer loop is a slower pointer i to compare with the faster pointer j
		for (int i = 0; i < words.length; i++) {
			//inner loop, whenever slower pointer i moves, faster pointer j will iterate to compare with the value between words[i] and words[j]
			for (int j = i + 1; j < words.length; j++) {
				// compare, case sensitive
				if (words[j].equals(words[i])) {
					// if it is equal and there is no items in duplicate list, the words will add into the duplicate list
					if (duplicate[0] == null) {
						duplicate[0] = words[i];
						// if the duplicate word has already in the duplicate list and the list is not null
					} else if (!duplicate[0].equals(words[i]) && duplicate[0] != null) {
						// add it to the next slot
						duplicate[1] = words[i];
					}
				} 
			} 
			
		
		}
		
		// iterate the duplicate list
		for (int i = 0; i < duplicate.length; i++) {
			// the duplicate list is not null, print out the list
			if (duplicate[i] != null) {
			System.out.println("Dupilicate word: " + duplicate[i]);
			// if the duplicate list is null, tell the user there is no duplicate word
			} else if (duplicate[0] == null) {
				System.out.println("No word is duplicated");
				break;
			}
		}
		
		//close the scanner
		scanner.close();
	}

}
