package exams.first;

import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
	    // use scanner to get users's input 
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("number?");

	    //validate for correct type of input
	    try {
	    	// assign user's input to variable number
	    	int number = Integer.parseInt(scanner.next());

		    //call print_triangle function
		    print_triangle(number);
		    
	    } catch (NumberFormatException e) {
	    	// asking for correct input type.
	    	System.out.print("valid input please.");
	    }

	    //close the scanner
	    scanner.close();

	  }

	     // To print filled triangle
	     static void print_triangle(int a)
	     {
	    	 
	    	 // create two variable b, and c for future loop use.
	    	 int b, c;
	    	 
	    	 // loop for rows.
	    	 for(b=0; b<a; b++)
	         {
	    		 // find out how many space needed for this row
	             for(c=0; c<b; c++)
	             {
	                 System.out.print(" ");
	             }
	             // find out how many * needed for this row
	             for(c=b;c<a;c++)
	             {
	                 System.out.print("*");
	             }
	             // next row
	             System.out.println("");
	         } 
	     
	     }
	}


