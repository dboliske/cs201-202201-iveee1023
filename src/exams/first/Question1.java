package exams.first;

import java.util.Scanner;

public class Question1 {

	public static void main(String[] args) {
		// ask user for his/her choice of integer
		System.out.println("What is your choice of number? please enter an integer.");
		// use scanner
		Scanner scanner = new Scanner(System.in);
		
		// input validation check
		try  {
			// assign the input to variable number
			int number = Integer.parseInt(scanner.next());
			// then add 65
			number += 65;
			
			//print out the answer of char version
			System.out.println("The answer is: " + (char)number);
			
		} catch(NumberFormatException e) {
			// if the input is not integer, ask the user to try again.
			System.out.println("Please enter a correct type, try again");
		}
		
		// close the scanner
		scanner.close();
		
	}
}
