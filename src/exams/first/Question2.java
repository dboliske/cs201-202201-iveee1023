package exams.first;

import java.util.Scanner;

public class Question2 {

	public static void main(String[] args) {
		// ask user for his/her choice of integer
		System.out.println("What is your choice of number? please enter an integer.");
		// use scanner to get user's input
		Scanner scanner = new Scanner(System.in);
		
		// validate user's correct type of input
		try {
			// assign user's input into variable number
			int number = Integer.parseInt(scanner.next());
			// selection loop
			if ( number%2 == 0 && number%3 == 0 ) {
				// print out "foobar" if it could be divide by both 2 and 3
				System.out.println("foobar");
			} else if ( number%2 == 0 ) {
				// print out "foo" if it could be divide by 2
				System.out.println("foo");
			} else if ( number%3 == 0) {
				// print out "bar" if it could be divide by 3
				System.out.println("bar");
			}
		} catch (NumberFormatException e) {
			// inform to user to try with correct input type
			System.out.println("Please enter a correct type of input");
		}
		
		
		// close the scanner
		scanner.close();
	}
	

}
