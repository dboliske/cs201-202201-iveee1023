# Final Exam

## Total

97/100

## Break Down

1. Inheritance/Polymorphism:    18/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                3/5
    - Methods:                  5/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  20/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  5/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           5/10
    - Results:                  5/5
5. Searching Algorithms:        19/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  4/5

## Comments

1. building and rootNumber should be protected. -2
2. ok.
3. ok.
4. ok
5. If the value is not present, it didn't print as required. -1 
