// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam




package exams.second;

public class ComputerLab extends Classroom {

	//private attribute
  private boolean computers;

  //default constructor function
  public ComputerLab() {
    super();
    this.computers = false;
  }

  
 //constructor function, it did not show on UML diagram, so I just commented out.
//  public ComputerLab(String building, String roomnumber, int seats, boolean computers) {
//    super(building, roomnumber, seats);
//    setComputers(computers);
//  }

  
  //setter function
  public void setComputers(boolean computers) {
    this.computers = computers;
  }

  
  //getter function
  public boolean hasComputer() {
    return computers;
  }

  // rewrite tostring function
  public String toString() {
    return super.toString() + "Computers: " + computers;
  }
}
