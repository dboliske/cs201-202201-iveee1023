// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam



package exams.second;

public class Classroom {

	//private attributes
    private String building;
    private String roomNumber;
    private int seats;

    
    //default constructor function
    public Classroom() {
      building = "shaw tower";
      roomNumber = "200";
      seats = 100;
    }

    //constructor function, it did not show on UML diagram, so I just commented out.
//    public Classroom(String building, String roomNumber, int seats) {
//      setBuilding(building);
//      setRoomNumber(roomNumber);
//      setSeat(seats);
//    }


    // setter functions
    public void setBuilding(String building) {
      this.building = building;
    }

    public void setRoomNumber(String number) {
      this.roomNumber = number;
    }

    public void setSeat(int seats) {
      if (seats >= 0) {
        this.seats = seats;
      }
    }

    //getter functions
    public String getBuilding() {
      return building;
    }

    public String getRoomNumber() {
      return roomNumber;
    }

    public int getSeat() {
      return seats;
    }

    //toString function
    public String toString() {
      return "Building:" + building + "Room Number: " + roomNumber + "Seat: " + seats;
    }

}
