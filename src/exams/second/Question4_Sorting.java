// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam





package exams.second;

public class Question4_Sorting {

	public static void main(String[] args) {
		
		String[] list = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		// calling selection sort method
		String[] sortedList = sort(list);
		
		for (String n : sortedList) {
			System.out.print(n + " ");
		}

	}

	public static String[] sort(String[] array) {
		for (int i=0; i < array.length - 1; i++) {
			//set a min value
			int min = i;
			for (int j = i + 1; j < array.length; j++) {
				// compare the current to min value, and save it if it is smaller
				if (array[j].compareToIgnoreCase(array[min]) < 0) {
					min = j;
				}
			}
			
			// swap
			if (min != i) {
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}

}
