// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam




package exams.second;

import java.util.Scanner;

public class Question5_Searching {
	
	public static void main(String[] args) {
		double[] lang = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Search term: ");
		try {
			double value = Double.parseDouble(scanner.nextLine());
			//set how far each step goes
			int step = (int)Math.sqrt(lang.length) -1;
			int index = search(lang, value, 0, step );
			if (index == -1) {
				System.out.println(value + " not found.");
			} else {
				System.out.println(value + " found at index " + index + ".");
			}
		} catch (Exception e) {
			System.out.println("Invalid Input");
		}
		
		
		//scanner close
		scanner.close();
	}
	
	
	
	public static int search(double[] array, double value, int prev,  int step) {

		//it the current step is smaller than the vale
		if (array[Math.min(step, array.length) - 1] < value ) {
			// set prev to the current step
			prev = step;
			// update step
			step += (int)Math.sqrt(array.length);
			
			//calling search function recursively
	        return search(array, value, prev, step);
	    } else {
	       
	    	//else if the value is smaller than the current step, then we do linear search to find target value;
		    for (int current = prev;
		    		current <= step && current < array.length; 
		    		current++) {
		       if (value == array[current]) {
		             return current;
		       }
		        }
	    }
	    
	    return -1;
	}
}