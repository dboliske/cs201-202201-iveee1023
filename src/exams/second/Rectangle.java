// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam






package exams.second;

public class Rectangle extends Polygon {
  
	// own attributes
  private double width;
  private double height;

  
  //constructor
  public Rectangle() {
    super();
    this.width = 1;
    this.height = 1;
  }

  
 //constructor function, it did not show on UML diagram, so I just commented out.
//  public Rectangle(String name, double width, double height) {
//    super(name);
//    this.width = width;
//    this.height = height;
//  }

  
  //setter functions
  public void setWidth(double width) {
    if (width >= 0) {
      this.width = width;
    }
  }

  
  public void setHeight(double height) {
    if (height >= 0) {
      this.height = height;
    }
  }

  
  //getter functions
  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }

  //tostring rewrite
  public String toString() {
    return super.toString() + "(" + width + ", " + height + ")";
  }

  
  //fill out abstract function
  public double area() {
    return height * width;
  }

  public double perimeter() {
    return 2.0 * (height + width);
  }

}
