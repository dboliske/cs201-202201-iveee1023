// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam





package exams.second;

public abstract class Polygon {
  
	//protected attribute 
  protected String name;

  //constructor 
  public Polygon() {
    name = "a";
  }

  //constructor function, it did not show on UML diagram, so I just commented out.
//  public Polygon(String name) {
//    this.name = name;
//  }

  
  //setter function
  public void setName(String name) {
    this.name = name;
  }

  
  //getter function
  public String getName() {
    return name;
  }

  //toString function
  public String toString() {
    return "Name: " + name;
  }

  //abstract functions
  public abstract double area();

  public abstract double perimeter();


}
