// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam





package exams.second;

import java.util.Scanner;
import java.util.ArrayList;


public class Question3_ArrayList {

	public static void main(String[] args) {

		//prepare scanner and empty arraylist
		Scanner scanner = new Scanner(System.in);
		ArrayList<Integer> list = new ArrayList<>();
		boolean run = true;
		//prepare two global varibles for result
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		while (run) {
			System.out.println("Give me a number or type 'Done.' ");
			String choice = scanner.nextLine();
			
			//ending situation
			if (choice.equals("Done")){
				// loop the list to get the min value
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i) < min) {
						min = list.get(i);
					}
				}

				// loop the list to get the max value
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i) > max) {
						max = list.get(i);
					}
				}

				System.out.println("the maximum number is: " + max);
				System.out.println("the minimal number is: " + min);

				run = false;
				break;
			}
				

			//not ending situations
			try {
				//add the input to the arraylist each time.
				int number = Integer.parseInt(choice);
				list.add(number);
			} catch (Exception e) {
				System.out.println("Invalid Input");
			} 


		}
		//close the scanner
		scanner.close();

	}
}
