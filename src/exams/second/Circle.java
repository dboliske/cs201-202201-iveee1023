// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 29, 2022; 	
// name of project: final exam


package exams.second;

public class Circle extends Polygon {

	//attribute
  private double radius;

  //contstructor
  public Circle() {
    super();
    this.radius = 1;
  }

  
  //constructor function, it did not show on UML diagram, so I just commented out.
//  public Circle(String name, double radius) {
//    super(name);
//    this.radius = radius;
//  }

  
  //setter function
  public void setRadius(double width) {
    if (radius >= 0) {
      this.radius = width;
    }
  
  }

  //getter function
  public double getRadius() {
    return this.radius;
  }

  
  //tostring function rewrite
  public String toString() {
    return super.toString() + "Radius: " + radius;
  }

  //fill out abstract functions
  public double area() {
    return Math.PI * radius * radius;
  }

  public double perimeter() {
    return 2.0 * Math.PI * radius;
  }

  
}
