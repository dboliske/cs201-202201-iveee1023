// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 4, 2022; 	
// name of project: labs.lab7




package labs.lab7;

public class Exercise3 {

	public static void main(String[] args) {
		
		double[] nums = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		// calling selection sort method
		double[] sortedNums = sort(nums);
		
		for (double n : sortedNums) {
			System.out.print(n + " ");
		}

	}

	public static double[] sort(double[] array) {
		for (int i=0; i<array.length - 1; i++) {
			//set a min value
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				// compare the current to min value, and save it if it is smaller
				if (array[j] < array[min]) {
					min = j;
				}
			}
			
			// swap
			if (min != i) {
				double temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}
}
