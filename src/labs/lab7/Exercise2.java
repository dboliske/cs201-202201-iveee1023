// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 4, 2022; 	
// name of project: labs.lab7




package labs.lab7;

public class Exercise2 {

	public static void main(String[] args) {
		
		String[] lang = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		
		// calling insertion sort method
		lang = sort(lang);
		
		for (String l : lang) {
			System.out.print(l + " ");
		}

	}
	
	public static String[] sort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			// insert the right word to the beginning
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}

}
