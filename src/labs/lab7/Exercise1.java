// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 4, 2022; 	
// name of project: labs.lab7




package labs.lab7;

public class Exercise1 {

	public static void main(String[] args) {
		
		int[] nums = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		// calling Bubble Sort method
		int[] sortedNums = sort(nums);
		
		for (int n : sortedNums) {
			System.out.print(n + " ");
		}
	}

	
	public static int[] sort(int[] array) {
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<array.length - 1; i++) {
				// compare, if the position is wrong
				if (array[i+1] < array[i]) {
					//save the current to temp
					int temp = array[i];
					// swap
					array[i] = array[i + 1];
					array[i + 1] = temp;
					done = false;
				}
			}
		} while (!done);
		
		return array;
	}
}
