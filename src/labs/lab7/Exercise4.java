// name: Yuan Fang
// course: CS201 sec: 03
// date: Apr. 4, 2022; 	
// name of project: labs.lab7




package labs.lab7;

import java.util.Scanner;

public class Exercise4 {

	public static void main(String[] args) {
		
		String[] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		//get user's input
		System.out.print("Search term: ");
		String value = input.nextLine();
		// calling binary search function
		int index = search(lang, 0, lang.length, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}
	
	
	public static int search(String[] array, int left, int right, String value) {
		
		// ending situation
		if (left >= right) {
            return -1;
        }
		
		// find midpoint, prevent overflow
		int mid = (right - left) / 2 + left;
		
		// if we find the target value
		if (value.equalsIgnoreCase(array[mid])) {
            return mid;
        } 
		
		// in case the value is larger the the midpoint
		if (array[mid].compareToIgnoreCase(value) < 0) {
			return search(array, mid + 1, right, value);
		} else {
			return search(array, left, mid, value);
		}
		
		
		
	}

}
