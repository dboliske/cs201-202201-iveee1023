package labs.lab3;

import java.io.File;
import java.util.Scanner;
import java.io.*;

public class Solution1 {

  public static void main(String[] args) throws IOException {
    //read the file, but it works while i am using absolute path.
    File file = new File("/Users/angrysoulsheep/git/cs201-202201-iveee1023/src/labs/lab3/grades.csv");
    Scanner input = new Scanner(file); // read data from file

    double sum = 0; //set the sum variable
    int student_count = 0; // set the student count

    //read the file if the file has next line available
    while (input.hasNextLine()) {
      
      String line = input.nextLine(); //read through the file, if there is a line, save it to line variable
      String grade = line.substring(line.indexOf(",")+1); //get the grade part from the string 

      double current = Double.parseDouble(grade); //convert the string to double and save it current
      sum += current; // updates sum
      student_count ++; // update student count

      

    }

    input.close(); //close the scanner for reading file.

    double average = sum / student_count; //calculate the average
    System.out.printf("The class average is: %.2f%n", average); //print out the average with two decimal place

  }
  
}
