// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 13, 2022; 	
// name of project: labs.lab3



package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution2 {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); //create scanner for user input

    double[] array = new double[1]; // smallest array if there is only one number.
    boolean run = true; // set the running status as true
    int count = 0; // counter

    // unless the run choose to quit, the loop will forever run.
    while  (run) {
      System.out.println("number?"); //ask for input
      String number = input.nextLine(); // save the input into variable number
      // selection loop
      switch (number) { 
      	// if the user enters done, the loop will stop.
      	case "done":
      		run = false;
      		break;
      	
      	default:
      		// if the number index exceed the length of a array, the array will extend one more space.
      		if (count >= array.length) {
      			//initiate a new temporary array with one larger space
      	        double[] temp_array = new double[array.length + 1];
      	        // copy the previous array to this new array
      	        for (int i = 0; i < array.length; i++) {
      	        	temp_array[i] = array[i];
      	        }
      	        
      	        // assign the new temporary array to the previous array.
      	        array = temp_array;
      	        // free the storage
				temp_array = null;
				
				// since there is one more space in here, assign the new number to the array.
      	        array[count] = Double.parseDouble(number);
      	        // counter increase by one
      	        count++;
      		} else  {
      			// this situation only happens at the first time, assign the number into the array.
      	        array[count] = Double.parseDouble(number);
      	        count++;
      		}
      		// alert the user that the number they choose has been written into the array.
      		System.out.println("saved");		
      }
    }

    // before saving to the text file, show the array to the user
    System.out.println("Here is your number list: ");
    for (int i = 0; i < array.length; i++) {
    	System.out.println("(" + (i+1) + "):  " + (array[i]));
    }
    
    // asking for the file name
    System.out.println("file name?");
    String filename = input.nextLine();
    
    // save it to the file
    try {
    	//open a new file, or write into a existing file
    	FileWriter f = new FileWriter("./src/labs/lab3/" + filename + ".txt");
    	
    	// write into the file, line by line.
    	for (int i = 0; i < array.length; i++) {
    		f.write(array[i] + "\n");
    	}
    	
    	// save and close.
    	f.flush();
    	f.close();
    	
    } catch (IOException e) {
    	// catch error
    	System.out.println(e.getMessage());
    }
    
    // notice the user that their file has been saved.
    System.out.println("Finished writing file, please check the file");

    // close the scanner
    input.close();
    
  }
}
  

