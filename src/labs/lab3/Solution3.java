// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 13, 2022; 	
// name of project: labs.lab3



package labs.lab3;

public class Solution3 {

	public static void main(String[] args) {
		
		// create a new array with question's 3 numbers
		int[] array = {72, 101, 108, 108, 111, 32, 101, 118, 101, 
		114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 
		32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 
		98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		// set the minimal number to the first one on the list
		int min = array[0];
		
		//looping the whole array to compare
		for (int i = 0; i < array.length; i++) {
			// if new current number is smaller than the current minimal number, replace it.
			if (array[i] < min) {
				min = array[i];
			}
		}
		
		// Announce the answer
		System.out.println("the smallest value is: " + min);
	}

}
