// name: Yuan Fang
// course: CS201 sec: 03
// date: Mar. 18, 2022; 	
// name of project: labs.lab6


package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounterApp {

	public static void main(String[] args) {
		// prepare empty ArrayList queue
		ArrayList<String> queue = new ArrayList<String>();
		//calling menu function
		menu(queue);
	}
	
	public static void menu(ArrayList<String> queue) {
		// scanner for users' future input
		Scanner scanner = new Scanner(System.in);
		boolean run = true;
		
		// menu choice
		do {
			System.out.println("Here is the menu: ");
			System.out.println("1. Add customer to queue");
			System.out.println("2. Help customer");
			System.out.println("3. Show queue");
			System.out.println("4. Exit");
			System.out.println("choice:");
			
			String choice = scanner.nextLine();
			switch (choice) {
				case "1":
					addCustomer(scanner, queue);
					break;
				case "2":
					removeCustomer(queue);
					break;
				case "3":
					showQueue(queue);
					break;
				case "4":
					run = false;
					break;
			    default:
			    	System.out.println("Invalid");	
			}
		} while(run);
		
		System.out.println("See you next time!");		
		scanner.close();
	}
	
	// add customer in queue function
	public static void addCustomer(Scanner scanner, ArrayList<String> queue) {
		System.out.println("What is your name?");
		String name = scanner.nextLine();
		
		queue.add(name);
		System.out.println("Added!");
	}
	
	
	// remove customer from the queue
	public static void removeCustomer(ArrayList<String> queue) {
		
		// if there has any customer in queue.
		if (queue.size() > 0) {
			System.out.println(queue.get(0) + " has finished.");
			// remove customer
			queue.remove(0);
		} else {
			// in case there has no customer in queue.
			System.out.println("There is no customer in queue.");
		}

		
	}
	
	
	// show queue status customer name and the position
	public static void showQueue(ArrayList<String> queue) {
		if (queue.size() > 0) {
			for (int i = 0; i < queue.size(); i++) {
				System.out.println(queue.get(i) + " Position: " + i);
			}
		} else {
			System.out.println("There is no customer in queue.");
		}
	}

}
