// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab2

package labs.lab2;

import java.util.Scanner;

public class Solution1 {
  
  public static void main(String[] args) {
    // use scanner to get users's input 
    Scanner scanner = new Scanner(System.in);
    System.out.println("number?");

    //assign the input to variable number 
    int number = Integer.parseInt(scanner.next());

    //call print_sqaure function
    print_square(number);

    //close the scanner
    scanner.close();

  }

     // To print filled rectangle
     static void print_square(int a)
     {
         int b, c;
  
         // Outer loop for rows, make sure the length.
         for (b = 0; b < a; b++) {
           // Inner loop for column, make sure the width.
           for(c = 1; c < a; c++) {
             System.out.print("* ");
           }
           System.out.println("* ");	
             }
         
     }
}
