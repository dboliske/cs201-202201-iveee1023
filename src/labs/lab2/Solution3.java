// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab2



package labs.lab2;


import java.util.Scanner;

public class Solution3 {
  
  public static void main(String[] args) {

    // set the program status to true for future while loop
    boolean run = true;
    // open a scanner for users's input
    Scanner scanner = new Scanner(System.in);


    // while loop start to run the program
    while(run) {

      // ask the user for their choice and assign their input to variable choice
      System.out.println("1.Say Hello, 2. Addition, 3. Multiplication, 4. Exit");
      int choice = Integer.parseInt(scanner.next());

      // check differeent choice case
      switch(choice) {
        case 1:
          //print out hello and break
          System.out.println("Hello");
          break;
        case 2:
          // print the addition 
          System.out.println("Give my a number");
          int num1 = Integer.parseInt(scanner.next());

          System.out.println("Give my another number");
          int num2 = Integer.parseInt(scanner.next());

          System.out.println("The sum is " + (num1 + num2));
          break;
        case 3:
          // print out the multiplication
          System.out.println("Give my a number");
          int num3 = Integer.parseInt(scanner.next());

          System.out.println("Give my another number");
          int num4 = Integer.parseInt(scanner.next());

          System.out.println("The sume is " + (num3 * num4));
          break;
        case 4:
          //print out bye when user wish to quit the program
          System.out.println("bye");
          run = false;
          break;
        default:
          //check for invalid input
          System.out.println("Please enter a valid number");
          break;          
      }
    }

  //close the scanner    
    scanner.close();

  }
}
