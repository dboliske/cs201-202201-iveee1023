// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab2


package labs.lab2;

import java.util.Scanner;

public class Solution2 {
  
  public static void main(String[] args) {

    // set the program running status
    boolean run = true;

    // open a scanner for future user's input
    Scanner scanner = new Scanner(System.in);

    //variable initialization for store grade sum and number of grades received.
    int sum_grade = 0;
    int counter = 0;

    //set a while loop 
    while(run) {

      // ask the user for an input and assign it to a variable
      System.out.println("Enter the grades you wish to add on calculation, or type -1 to leave the program ");
      int grade = Integer.parseInt(scanner.next());

      //if loop to check user's input
      if (grade != -1 && grade <= 100) {
        // add the grade input to sum grade and output his/her current average
        sum_grade += grade;
        counter += 1;
        System.out.println("grade average up to now is: " + sum_grade / counter);
      } else if (grade == -1 ) {
        // case when user wish to quit the program
        run = false;
      } else {
        // check for invalid input
        System.out.println("Invalid grade");
      }
    }

    //close the scanner
    scanner.close();

  }
}
