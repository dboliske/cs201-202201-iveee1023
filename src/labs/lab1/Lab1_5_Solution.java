// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;


import java.util.Scanner;

//testtable
//value (length, width, height)
// 3,3,3  Result: 7776.50
// 14,23,35 Result: 465725.81
// 0,2,3 Result: Invalid program


public class Lab1_5_Solution {
  

  public static void main(String[] args) {
    // use scanner for getting user's input 
    Scanner scanner = new Scanner(System.in);
		
		// ask the user for length
		System.out.print("length in inches: ");
		Integer length = scanner.nextInt();
		
     // ask the user for width
		System.out.print("width in inches: ");
		Integer width = scanner.nextInt();

   // ask the user for depth
		System.out.print("depth in inches: ");
		Integer depth = scanner.nextInt();

    //close the Scanner
    scanner.close();


    //check for valid input
		
    if (length >0 && width > 0 && depth > 0){
      //calculate the the square inch to square feet
      double squareFeet = (length*width + length*depth + depth*width) * 2 /0.006944;

		  // print out 
		  System.out.printf("the amount of wood (square feet) needed to make the box: %.2f%n" , squareFeet);
		} else{
      System.out.println("invalid input, Please Run the program again");
      return;
    }

  
	}

  
}

