// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;

import java.util.Scanner;

public class Lab1_2_Solution {
  
  public static void main(String[] args) {

    //get user's age
    System.out.println("How old are you?  ");
    // use the scanner
    Scanner scanner = new Scanner(System.in);

    //assign user's first answer to variable user_age
    int user_age = Integer.parseInt(scanner.next());

    //get user's data age
    System.out.println("How old is your father?");
    // assign user's next answer to variable user_dad_age
    int user_dad_age = Integer.parseInt(scanner.next());

    //get user's birth year
    System.out.println("How year you born?");
    // assign user's next answer to varibale birthyear
    int birthyear = Integer.parseInt(scanner.next());

    //get users height in inches
    System.out.println("How tall are you in inches?");
    // assign user's height in inches to varibale height_inch
    int height_inch = Integer.parseInt(scanner.next());




    //calculate users age subtracted from father's age
    System.out.println("Your age subtracted from your father's age is " + (user_dad_age - user_age) + ".");
    //Your birth year multiplied by 2
    System.out.println("Your birth year multiplied by 2 is  " + (birthyear * 2) + ".");
    //Convert user's height to cm; 1 inch = 2.54cm
    System.out.println("Your height in cm is " + (height_inch * 2.54) + ".");
    //Convert user's height to foot; 1 foot = 12 inches
    System.out.println("your height in foot is " + (((int)(height_inch / 12)) + " feet " + (height_inch % 12) + " inches." ));

    //close the scanner
    scanner.close();

  }
}