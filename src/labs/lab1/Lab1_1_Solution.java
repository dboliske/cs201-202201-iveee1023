// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;

import java.util.Scanner;

public class Lab1_1_Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Writing a Java program that will prompt a user for a name
		System.out.println("What is your name: ");
		
		// Use scanner and Get the name
		Scanner user_name = new Scanner(System.in);
		
		// echo the name
		System.out.println("Hi! " + user_name.next());

		//close the Scanner
		user_name.close();
	}

}
