// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;

import java.util.Scanner;


// test table 
//----------------------------------------------------------------
//                      Fahrenheit                  Celsius
//----------------------------------------------------------------
//  LOW                   -260                      -162.22
//----------------------------------------------------------------
//  MEDIUM                 100                       37.78
//----------------------------------------------------------------
//  HIGH                   1000                     538.78
//----------------------------------------------------------------



public class Lab1_4_Solution {

  public static void main(String[] args) {
      // use scanner for getting user's input 
      Scanner scanner = new Scanner(System.in);

      // Ask the user for a Fahrenheit temperature and assign the value to variable fahrenheit
      System.out.print("Give me a temperature in Fahrenheit: ");
      double fahrenheit = scanner.nextFloat();

      // print out
      System.out.printf("F° : " + fahrenheit + "   C° : %.2f%n", (fahrenheit - 32)/1.8);

      
      // Ask the user for a Fahrenheit temperature
      System.out.print("Give me a temperature in Celsius: ");
      double celsius = scanner.nextFloat();
      double fahrenheit1 = celsius * 1.8 + 32;

      // System.out.println(ff);

      // print out
      System.out.printf("F° : %.2f  C° : %.2f%n",  fahrenheit1, celsius);
      

      //close the scanner
      scanner.close();


  }
  
}
