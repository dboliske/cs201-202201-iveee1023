// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;

import java.util.Scanner;

public class Lab1_3_Solution {

  public static void main(String[] args) {

    //ask user for his/her first name
    System.out.println("What is your first name?");
    //use scanner
    Scanner scanner = new Scanner(System.in);
    //print out the first initial;
    System.out.println("Your initial for your first name is:  " + scanner.next().charAt(0));

    scanner.close();
    

  }
  
}
