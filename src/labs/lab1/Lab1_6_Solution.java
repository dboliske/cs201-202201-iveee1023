// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 6, 2022; 	
// name of project: labs.lab1

package labs.lab1;

import java.util.Scanner;

//test table
//value 
// 5  Result: 12.7
// 76 Result: 193.04
// 0 Result: 0.00
// 3.5 Result: 8.89

public class Lab1_6_Solution {

  public static void main(String[] args) {
    // use scanner for getting user's input 
    Scanner scanner = new Scanner(System.in);

    // ask the user for length
		System.out.print("inches: ");
		float inch = scanner.nextFloat();

    // print out
    System.out.printf("centimeters: %.2f%n", inch * 2.54);

    //close the scanner
    scanner.close();
  }
  
}
