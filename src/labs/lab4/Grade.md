# Lab 4

## Total

22/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        6/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         3/3

## Comments

1. None of your non-default constructors validate the parameters prior to assigning them to the instance variables
2. None of your mutator methods validate the parameters prior to assigning them to the instance variables
3. In both your PhoneNumber and Potion classes, you do not follow the UML diagram
