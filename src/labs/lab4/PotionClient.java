// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		// initiate a new instance of Potion class with default value;
		Potion p1 = new Potion();
		
		// print out p1's all variable
		System.out.println(p1.toString());
		// set "bob" to p1's name variable
		p1.setName("bob");
		// print out p1's all variable
		System.out.println(p1.toString());
		// set 2 to p1's strength variable
		p1.setStrength(2);
		// print out p1's all variable
		System.out.println(p1.toString());
		// print out name variable
		System.out.println(p1.getName());
		// print out strength variable
		System.out.println(p1.getStrength());
		
		// check if the variable strength is a valid
		System.out.println(p1.validStrength());
		
		// initiate another new instance of Potion class with customized variable;
		Potion p2 = new Potion("bob", 2);
		// compare if p1 is equals to p2
		System.out.println(p1.equals(p2));
	}

}
