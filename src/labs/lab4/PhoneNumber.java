// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class PhoneNumber {

	// class variable country code, areacode , number'
	private String countryCode;
	private String areaCode;
	private String number;
	
	
	// construction method - default
	public PhoneNumber() {
		// default value
		countryCode = "001";
		areaCode = "604";
		number = "1234567";
	}
	
	
	// construction method with arguments.
	public PhoneNumber(String cCode, String aCode, String num) {
		// assign the arguments value to the class variables;
		countryCode = cCode;
		areaCode = aCode;
		number = num;
	}
	
	
	// getter method for countrycode
	public String getCountryCode() {
		return countryCode;
	}
	
	// getter method for areacode
	public String getAreaCode() {
		return areaCode;
	}
	
	
	// getter method for number
	public String getNumber() {
		return number;
	}
	
	
	// setter method for countrycode
	public void setCountryCode(String cCode) {
		// assign the cCode value to variable countrycode
		countryCode = cCode;
	}
	
	// setter method for areacode
	public void setAreaCode(String aCode) {
		// assign the aCode value to variable areacode
		areaCode = aCode;
	}
	
	
	// setter method for number
	public void setNumber(String num) {
		// assign the num value to variable number
		number = num;
	}
	
	// toString method, return all variable values.
	public String toString() {
		return countryCode + areaCode + number;
	}
	
	
	// check if the current area code is valid.
	public boolean validAreaCode() {
		// areacode length should be 3 
		if (areaCode.length() != 3) {
			return false;
		}
		
		return true;
	}
	
	
	// check if the current number is valid
	public boolean validNumber() {
		// number length should be 7 
		if (number.length() != 7) {
			return false;
		}
		
		return true;
	}
	
	
	// check if this instance is same as the other instance of PhoneNumber.
	public boolean equals(PhoneNumber p) {
		if (this.countryCode == p.countryCode && this.areaCode == p.areaCode && this.number == p.number) {
			return true;
		}
		
		return false;
	}
	
}

