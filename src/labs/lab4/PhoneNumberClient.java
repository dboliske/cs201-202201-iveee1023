// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		// initiate a new instance of PhoneNumber class with default value;
		PhoneNumber p1 = new PhoneNumber();
		
		// print out p1's all variable value
		System.out.println(p1.toString());
		// set the countrycode to "002";
		p1.setCountryCode("002");
		// print out p1's all variable value
		System.out.println(p1.toString());
		// set the areacode to "605"
		p1.setAreaCode("605");
		// print out p1's all variable value
		System.out.println(p1.toString());
		// set number to "2345678"
		p1.setNumber("2345678");
		// print out p1's all variable value
		System.out.println(p1.toString());
		// print out p1's current countrycode
		System.out.println(p1.getCountryCode());
		// print out p1's current areacode
		System.out.println(p1.getAreaCode());
		// print out p1's current number
		System.out.println(p1.getNumber());
		
		// check if p1's areacode is valid
		System.out.println(p1.validAreaCode());
		// check if p1's number is valid
		System.out.println(p1.validNumber());
		
		//initiate another new instance of PhoneNumber class with customized variable;
		PhoneNumber p2 = new PhoneNumber("002", "605", "2345678");
		// check if p1 is equals to p2
		System.out.println(p1.equals(p2));
		
	}

}
