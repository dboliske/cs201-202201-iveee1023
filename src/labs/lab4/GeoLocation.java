// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class GeoLocation {

	// class variable latitude and longitude.
	private double lat;
	private double lng;
	
	
	// construction method - default
	public GeoLocation() {
		
		// default variable lat and lng
		lat = 30;
		lng = 30;
	}
	
	
	// construction method which takes argument.

	public GeoLocation(double lat1, double lng1) {
		
		// assign the given arguments to the class variable
		lat = lat1;
		lng = lng1;
	}
	
	
	// getter method, return lat value;
	public double getLat() {
		return lat;
	}
	
	
	// getter method, return lng value;
	public double getLng() {
		return lng;
	}
	
	// setter method, assign argument lat1 to the class variable lat;
	public void setLat(double lat1) {
		this.lat = lat1;
	}
	
	// setter method, assign argument lng1 to the class variable lng;
	public void setLng(double lng1) {
		// assign argument lng1 into variable lng
		this.lng = lng1;
	}
	
	
	// toString method, return all class variable
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	
	
	// check if the latitute is in between -90 to 90;
	public boolean validLat(double lat1) {
		if (lat1 >= -90 && lat1 <= 90) {
			return true;
		} else {
			return false;
		}
	}
	
	
	// check if the longitude is in between -180 to 180;
	public boolean validLng(double lng1) {
		if (lng1 >= -180 && lng1 <= 180) {
			return true;
		} else {
			return false;
		}
	}
	
	// check if this instance is same as the other instance of GeoLocation.
	public boolean equals(GeoLocation g) {
		if (this.lat != g.lat) {
			return false;
		} else if (this.lng != g.lng) {
			return false;
		}
		
		return true;
	}
}


