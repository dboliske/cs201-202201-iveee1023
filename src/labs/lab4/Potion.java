// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class Potion {

	// create class variable name and strength;
	private String name;
	private double strength;
	
	// Constructor method - default
	public Potion() {
		// default value
		name = "alice";
		strength = 1;
	}
	
	// constructor method with arguments
	public Potion(String nam, double str) {
		// assign arguments to variable
		name = nam;
		strength = str;
	}
	
	// getter method for name
	public String getName() {
		return name;
	}
	
	// getter method for strength
	public double getStrength() {
		return strength;
	}
	
	// setter method for name variable
	public void setName(String nam) {
		name = nam;
	}
	
	// setter method for strength variable
	public void setStrength(double str) {
		strength = str;
	}
	
	// toString method which will print out all variable's value
	public String toString() {
		return name + strength;
	}
	
	// check if the strength variable is in between 0 and 10
	public boolean validStrength() {
		if (strength >= 0 && strength <= 10 ) {
			return true;
		}
		
		return false;
	}
	
	// // check if this instance is same as the other instance of Potion.
	public boolean equals(Potion p) {
		if (this.name == p.name && this.strength == p.strength) {
			return true;
		}
		
		return false;
	}
	
	
}

