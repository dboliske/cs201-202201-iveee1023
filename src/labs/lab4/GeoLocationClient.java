// name: Yuan Fang
// course: CS201 sec: 03
// date: Feb. 20, 2022; 	
// name of project: labs.lab4



package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		// initiate a new instance of Geolocation with default value;
		GeoLocation g1 = new GeoLocation();
		
		// print out all variable of g1
		System.out.println(g1.toString());
		// set g1 lat variable to value 50
		g1.setLat(50);
		// print out all variable of g1
		System.out.println(g1.toString());
		// set g1 lng variable to 50
		g1.setLng(50);
		// print out all variable of g1
		System.out.println(g1.toString());
		// print out the value of lat
		System.out.println(g1.getLat());
		// print out the value of lng
		System.out.println(g1.getLng());
		
		// check if g1's lat is a valid number
		System.out.println(g1.validLat(40));
		// check if g1's lng is a valid number
		System.out.println(g1.validLng(-40));
		
		// initiate another new instance of Geolocation with customized variable
		GeoLocation g2 = new GeoLocation(50, 50);
		// check if g1 is equal to g2
		System.out.println(g1.equals(g2));
		
		
			
		
		
		
		
	}

}
