// name: Yuan Fang
// course: CS201 sec: 03
// date: Mar. 06, 2022; 	
// name of project: labs.lab5



package labs.lab5;

public class CTAStation extends GeoLocation {
	
	
	// class variable name, location, wheelchair, and open 
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	
	// default constructor
	public CTAStation() {
		super();
		this.name = "Downtown";
		this.location = "Vancouver";
		this.wheelchair = true;
		this.open = true;
	}
	
	
	
	// customize constructor
	public CTAStation(
			String name, 
			double lat, 
			double lng, 
			String location, 
			boolean wheelchair, 
			boolean open) {
		// set parent constructor and set variables
		super(lat, lng);
		this.name = name;
		setName(name);
		this.location = location;
		setLocation(location);
		this.wheelchair = wheelchair;
		setWheelchair(wheelchair);
		this.open = open;
		setOpen(open);
	}
	
	// setter method, assign argument name to name
	public void setName(String name) {
		this.name = name;
	}
	
	// setter method, assign argument location to location
	public void setLocation(String location) {
		this.location = location;
	}
	
	// setter method, assign argument wheelchair status to wheelchair
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	// setter method, assign argument open status to open
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	
	// access to the name
	public String getName() {
		return this.name;
	}
	
	// access to the location
	public String getLocation() {
		return this.location;
	}
	
	// access to the wheelchair status
	public boolean hasWheelchair() {
		return this.wheelchair;
	}
	
	// access to the open status
	public boolean isOpen() {
		return this.open;
	}
	
	
	@Override
	// override parent function
	public boolean equals(Object obj) {
		// check if the object have the same parent
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof CTAStation)) {  // check if the object is the instance of this class
			return false;
		}
		
		
		// class convert, and the detail variable;
		CTAStation c = (CTAStation)obj;
		if (this.name != c.getName()) {
			return false;
		} else if (this.location != c.getLocation()) {
			return false;
		} else if (this.wheelchair != c.hasWheelchair()) {
			return false;
		} else if (this.open != c.isOpen()) {
			return false;
		} 
		return true;
	}
	
	@Override
	// override to string function, because we have more variables need to be displayed.
	public String toString() {
		return name + " " + location + " " + wheelchair + " " + open + " " + super.toString();
	}
	
	
	
}
