// name: Yuan Fang
// course: CS201 sec: 03
// date: Mar. 06, 2022; 	
// name of project: labs.lab5



package labs.lab5;

public class GeoLocation {

	// class variable latitude and longitude.
	private double lat;
	private double lng;
	
	
	// construction method - default
	public GeoLocation() {
		
		// default variable lat and lng
		lat = 30;
		lng = 30;
	}
	
	
	// construction method which takes argument.

	public GeoLocation(double lat1, double lng1) {
		// check for validity and assign the given arguments to the class varibale

		this.lat = lat1;
		setLat(lat);
		this.lng = lng1;
		setLng(lng);

	}
	
	
	// getter method, return lat value;
	public double getLat() {
		return lat;
	}
	
	
	// getter method, return lng value;
	public double getLng() {
		return lng;
	}
	
	// setter method, assign argument lat1 to the class variable lat;
	public void setLat(double lat1) {
		if (validLat(lat1)) {
			this.lat = lat1;
		} 
	}
	
	// setter method, assign argument lng1 to the class variable lng;
	public void setLng(double lng1) {
		// assign argument lng1 into variable lng;
		if (validLng(lng1)) {
			this.lng = lng1;
		}
	}
	
	
	// toString method, return all class variable
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	
	
	// check if the latitude is in between -90 to 90;
	public boolean validLat(double lat1) {
		if (lat1 >= -90 && lat1 <= 90) {
			return true;
		} else {
			return false;
		}
	}
	
	
	// check if the longitude is in between -180 to 180;
	public boolean validLng(double lng1) {
		if (lng1 >= -180 && lng1 <= 180) {
			return true;
		} else {
			return false;
		}
	}
	
	// check if this instance is same as the other instance of GeoLocation.
	public boolean equals(Object obj) {
		if (!(obj instanceof GeoLocation)) {
			return false;
		}
		
		GeoLocation g = (GeoLocation)obj;
		
		if (this.lat != g.getLat() || this .lng != g.getLng() ) {
			return false;
		} 
		return true;
	}
	
	
	// calculate the distance between a given geolocation with this geolocation
	public double calcDistance(GeoLocation g) {
		if (this.equals(g)) {
			return 0;
		} else {
			double distance = Math.sqrt(Math.pow(this.lat - g.getLat(), 2) + Math.pow(this.lng - g.getLng(), 2));
			return distance;
		}
	}
	
	
	// calculate the distance between given latitude and longitude
	public double calcDistance(double lat, double lng) {
		if (this.lat == lat && this.lng == lng) {
			return 0;
		} else {
			double distance = Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
			return distance;
		}
	}
	
	 
}


