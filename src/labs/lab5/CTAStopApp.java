// name: Yuan Fang
// course: CS201 sec: 03
// date: Mar. 06, 2022; 	
// name of project: labs.lab5



package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {

	public static void main(String[] args) {
		
		// read from given path, calling readfile function
		CTAStation[] data = readFile("./src/labs/lab5/CTAStops.csv");
		try {
			menu(data);
		} catch (Exception e){
			System.out.println("Loading Error");
		}
		

	}
	
	
	// menu function, has received the array of CTAStation.
	public static void menu(CTAStation[] data) {
		
		Scanner scanner = new Scanner(System.in); // open a scanner for user's input
		boolean run = true;
		// unless the run == false, it will forever looping.
		do {
			// choices
			System.out.println("Here is the menu: ");
			System.out.println("1. Display all station name");
			System.out.println("2. Display station with/without wheelchair supply");
			System.out.println("3. Find Nearest Station");
			System.out.println("4. Exit the program");
			System.out.println("choice:");
			String choice = scanner.nextLine();
			switch (choice) {
				case "1":
					displayStationNames(data); //display all the station names
					break;
				case "2":
					displayByWheelchair(scanner, data); // display all the station has wheelchair
					break;
				case "3":
					displayNearest(scanner, data);  // display nearest station
					break;
				case "4":
					run = false; // quit
					break;
				default:
					System.out.println("Invalid");	// if the input is differet than 1234
			}		
		} while(run);
		
		System.out.println("See you next time!");		
		scanner.close();
	}
	
	
	// display all the station names, need to take stations data.
	public static void displayStationNames(CTAStation[] stations) {
		// looping through stations array, and print the name;
		for (int i=0; i<stations.length; i++) {
			if ( stations[i] != null) {
				System.out.println((i+1) + ": " + stations[i].getName());
			}
		};
	}

	// display all the station names has wheelchair, need to take stations data.
	public static void displayByWheelchair(Scanner input, CTAStation[] stations) {
		// ask if they need wheelchair, and save the answer to the variable answer	
		boolean run = true;
		//Checks that the input is 'y' or 'n', continues to prompt the user for 'y' or 'n' until one has been entered
		do {
			System.out.println("Need wheelchair? y/n ");
			String answer = input.nextLine();
			// if Y, loop the stations and check for their wheelchair status, but only print out those has wheelchair
			if (answer.equalsIgnoreCase("y")) {
				CTAStation[] c = new CTAStation[stations.length]; // save stations with wheelchair into this array
				int count = 0; //for resize use
				
				System.out.println("Here are the stations with wheelchair: ");
				for (int i=0; i<stations.length; i++) {
					if (stations[i].hasWheelchair()) {
						System.out.println((i+1) + ": " + stations[i].getName());
						c[count] = stations[i];
						count++;
					}
				}
				
				c = resize(c, count); // resize

				if(count == 0) { // if requirement is not meet in any of the station
					System.out.println("These is not station provide wheelchair");
				}
				run = false;
			} else if (answer.equalsIgnoreCase("n")) {
				CTAStation[] c = new CTAStation[stations.length];
				int count = 0;
				// if N, loop the stations and check for their wheelchair status, but only print out those does not has wheelchair
				System.out.println("Here are the stations without wheelchair: ");
				for (int i=0; i<stations.length; i++) {
					if (!stations[i].hasWheelchair()) {
						System.out.println((i+1) + ": " + stations[i].getName());
						c[count] = stations[i];
						count++;
					}
				}
				
				c = resize(c, count);

				if(count == 0) {
					System.out.println("All stations provide wheelchair");
				}
				run = false;
			} else {
				System.out.println("Invalid input"); // validity check
			}
		} while (run);

	}
	
	// display nearest station according to user's input
	public static void displayNearest(Scanner input, CTAStation[] stations) {
		System.out.println("your latitute?"); // ask the user for their latitute
		double lat = 0;
		double lng = 0;
		
		// validity check
		try {
			lat = Double.parseDouble(input.nextLine()); // save to variable lat
		} catch (Exception e) {
			System.out.println("please enter a valid number");
		}
		
		System.out.println("your longtitute?");
		try {
			lng = Double.parseDouble(input.nextLine()); //save to variable lng
		} catch (Exception e) {
			System.out.println("please enter a valid number");
		}
		
		
		// initiate the nearest distance as a large number
		double nearest_distance = Integer.MAX_VALUE;
		CTAStation nearest_c = null; // initiate CTAStation for future use.
		for (int i=0; i < stations.length; i++) {
			double temp_distance = stations[i].calcDistance(lat, lng); //get correct distance

			if (temp_distance < nearest_distance) {
				// if the current distance smaller the nearest distance, update it. 
				nearest_distance = temp_distance;
				nearest_c = stations[i]; // update the nearest station
			}
		}
		
		// print out the result;
		System.out.println("The nearest station is : " + nearest_c.getName());
		System.out.print(nearest_c.hasWheelchair() ? "It provides Wheelchair" : "It does not provides wheelchair");
		System.out.print(", and it is " );
		System.out.println(nearest_c.isOpen() ? "open." : "not open.");
		
		
	}
	
	
	
	// convert csv data to an array of CTAStation
	public static CTAStation[] readFile(String Filename) {
		// initiate a empty array
		CTAStation[] stations = new CTAStation[5];
		// set count for future resize use.
		int count = 0;
		// read through files.
		try {
			File f = new File(Filename); //initiate a file object f
			Scanner file = new Scanner(f); // use scanner to read through the file f
			while(file.hasNextLine()) {
				// save a row of data into a string 
				String line = file.nextLine();
				String[] values = line.split(",");  //split the string by ","
				// if the first object is equal to Name, then we recognized it as header and skip
				if (values[0].equals("Name")) { 
					continue;
				} else {
					// form a CTAStation object
					CTAStation c = new CTAStation(
							values[0], 
							Double.parseDouble(values[1]), 
							Double.parseDouble(values[2]), 
							values[3], 
							Boolean.parseBoolean(values[4]),
							Boolean.parseBoolean(values[5]));
					
					// if this object is about to exceed the array, double the array size
					if (count == stations.length) {  
						stations = resize(stations, count * 2); 
					}
					
					// add the ctastation instance into the stations array with the right slot
					stations[count] = c;
					count++;
				}
			}
			
			// close the file
			file.close();
			stations = resize(stations, count); // if the length of the array larger than the space need, resize it again.
		} catch (FileNotFoundException e) {
			System.out.println("file not found"); // catch error
		} catch (Exception e) {
			System.out.println("error");
		}
		
		
		return stations; // return stations array, the data has been parsed.
	}
	
	// resize function
	public static CTAStation[] resize(CTAStation[] data, int size)
	{
		CTAStation[] temp = new CTAStation[size];  // create a temp array for future copy use
		int limit = data.length > size ? size : data.length; 
		for(int i=0; i<limit; i++) {
			temp[i] = data[i]; // copy the temp array
		}

		return temp;
	}
	
	
	
	
	
	
	
}
